<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com" version="1.0">
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:output method="xml"/>
	<xsl:template match="*">
		<data_xml>
			<document document_var_name="transaction">
			<xsl:choose>
				 <xsl:when test = "//sf2:size > 0">
						<isAENameValid_t>true</isAENameValid_t>
						<validateAEName_t>N</validateAEName_t>
						
						<aEName_t>
							<xsl:value-of select = "//sf:Name" />
						</aEName_t>
										
						<aEFirstName_t>
							<xsl:value-of select = "//sf:FirstName"/>
						</aEFirstName_t>
						
						<aELastName_t>
							<xsl:value-of select = "//sf:LastName"/>
						</aELastName_t>
						
						<nLSAEFirstName_t>
							<xsl:value-of select = "//sf:NLS_First_Name__c"/>
						</nLSAEFirstName_t>
						
						<nLSAELastName_t>
							<xsl:value-of select = "//sf:NLS_Last_Name__c"/>
						</nLSAELastName_t>
						
						<nLSAELanguageCode_t>
							<xsl:value-of select = "//sf:NLS_Language_Code__c"/>
						</nLSAELanguageCode_t>
						
						<aEFormalName_t>
							<xsl:value-of select = "//sf:Formal_Name__c"/>
						</aEFormalName_t>
						
						<aETelephone_t>
							<xsl:value-of select = "//sf:Phone"/>
						</aETelephone_t>
						
						<aEEmail_t>
							<xsl:value-of select = "//sf:Email"/>
						</aEEmail_t>
				</xsl:when>
				<xsl:otherwise> <!-- Entered AE Name is not available in SFA-->
					<isAENameValid_t>false</isAENameValid_t>
				</xsl:otherwise>
			</xsl:choose>
			</document>
		</data_xml>
	</xsl:template>
</xsl:stylesheet>