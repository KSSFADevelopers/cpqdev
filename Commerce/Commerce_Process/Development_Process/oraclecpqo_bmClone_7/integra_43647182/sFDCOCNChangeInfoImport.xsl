<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com" version="1.0">
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:output method="xml"/>
	<xsl:template match="*">
		<data_xml>
			<document document_var_name="transaction">
			 <xsl:if test = "//sf2:size > 0">
				<!-- No Need to set Ship To Company Name and Customer Account if Quote to Address is getting changed. -->
					<!--<customerCompanyName_t>
						<xsl:value-of select="//sf:Name"/>
					</customerCompanyName_t>
					
					<customerAccount_t>
						<xsl:value-of select="//sf:Name"/>
					</customerAccount_t> -->
					
					<accountName_t>
						<xsl:value-of select="//sf:Name"/>
					</accountName_t>
				
				<localLanguageCustomerName_t>
					<xsl:value-of select="//sf:Alternate_Account_Name__c"/>
				</localLanguageCustomerName_t>
				
				<oCN_quote>
					<xsl:value-of select="//sf:OCN__c "/>
				</oCN_quote>

				<sFDCPaymentTerms_t>
					<xsl:value-of select="//sf:Payment_Terms__c"/>
				</sFDCPaymentTerms_t>

				<customerCategory_t>
					<xsl:value-of select="//sf:Account_Class__c"/>
				</customerCategory_t>

				<salesChannel_t>
					<xsl:value-of select="//sf:Sales_Channel__c"/>
				</salesChannel_t>
				
				<eRPSalesChannel_t>
					<xsl:call-template name="salesChannelTransformation">
						<xsl:with-param name="sfdcSalesChannel" select="//sf:Sales_Channel__c"/>
					</xsl:call-template>
				</eRPSalesChannel_t>

				<customerScreeningInfo_t>
					<xsl:value-of select="//sf:Customer_Screening__c"/>
				</customerScreeningInfo_t>
				
				<incoterm_quote>
					<xsl:value-of select="//sf:Incoterm__c"/>
				</incoterm_quote>
			 </xsl:if>
			</document>
		</data_xml>
	</xsl:template>
	
	<!--Defect# 1558, Updating eRPSalesChannel_t based upon provided mapping -->
	<xsl:template name = "salesChannelTransformation">
		<xsl:param name="sfdcSalesChannel"/>
		<xsl:choose>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_UNAUTHORIZED_RESELLER'">INDIRECT_UNAUTHORIZED_RESELLER</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'FTO'">FTO</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_SP'">INDIRECT_SP</xsl:when> 
			<xsl:when test="$sfdcSalesChannel = 'DIRECT_LEASING'">DIRECT_LEASING</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_SI_DIRECT'">INDIRECT_SI</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_VAR'">INDIRECT_VAR</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_VAR_DIRECT'">INDIRECT_VAR</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_SI'">INDIRECT_SI</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_OEM_DIRECT'">INDIRECT_OEM</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_OEM'">INDIRECT_OEM</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_IDR'">INDIRECT_IDR</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_DIST_MASTER'">INDIRECT_DIST_MASTER</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_ISS'">INDIRECT_ISS</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_DIST'">INDIRECT_DIST</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_ATP'">INDIRECT_ATP</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'DIRECT'">DIRECT</xsl:when>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>