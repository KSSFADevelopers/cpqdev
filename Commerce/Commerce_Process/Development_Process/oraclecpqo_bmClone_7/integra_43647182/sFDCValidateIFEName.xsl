<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com" version="1.0">
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:output method="xml"/>
	<xsl:template match="*">
		<data_xml>
			<document document_var_name="transaction">
			<xsl:choose>
				 <xsl:when test = "//sf2:size > 0">
						<isIFENameValid_t>true</isIFENameValid_t>
						<validateIFEName_t>N</validateIFEName_t>
						
						<iFEName_t>
							<xsl:value-of select = "//sf:Name" />
						</iFEName_t>
										
						<iFEFirstName_t>
							<xsl:value-of select = "//sf:FirstName"/>
						</iFEFirstName_t>
						
						<iFELastName_t>
							<xsl:value-of select = "//sf:LastName"/>
						</iFELastName_t>
						
						<nLSIFEFirstName_t>
							<xsl:value-of select = "//sf:NLS_First_Name__c"/>
						</nLSIFEFirstName_t>
						
						<nLSIFELastName_t>
							<xsl:value-of select = "//sf:NLS_Last_Name__c"/>
						</nLSIFELastName_t>
						
						<nLSIFELanguageCode_t>
							<xsl:value-of select = "//sf:NLS_Language_Code__c"/>
						</nLSIFELanguageCode_t>
						
						<iFEFormalName_t>
							<xsl:value-of select = "//sf:Formal_Name__c"/>
						</iFEFormalName_t>
						
						<iFETelephone_t>
							<xsl:value-of select = "//sf:Phone"/>
						</iFETelephone_t>
						
						<iFEEmail_t>
							<xsl:value-of select = "//sf:Email"/>
						</iFEEmail_t>
				</xsl:when>
				<xsl:otherwise> <!-- Entered IFE Name is not available in SFA-->
					<isIFENameValid_t>false</isIFENameValid_t>
				</xsl:otherwise>
			</xsl:choose>
			</document>
		</data_xml>
	</xsl:template>
</xsl:stylesheet>