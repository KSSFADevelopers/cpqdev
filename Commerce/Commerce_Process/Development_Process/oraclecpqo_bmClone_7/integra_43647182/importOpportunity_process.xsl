<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com" version="1.0">
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:output method="xml"/>
	<xsl:template match="*">
		<data_xml>
			<document document_var_name="transaction">
			 <xsl:if test="//sf2:size &gt; 0">
				<sfaStage_t>
					<xsl:value-of select="//sf:StageName"/>
				</sfaStage_t>
				
				<!-- Quote is created by Opportunity -->
				<xsl:if test="//sf:type = 'Opportunity'">
					<opportunityName_t>
						<xsl:value-of select="//sf:Name"/>
					</opportunityName_t>
					<currencyDropDown_t>
						<xsl:value-of select="//sf:Quote_Currency__c"/>
					</currencyDropDown_t>
					
					<industrySegment_t>
						<xsl:value-of select="//sf:Industry_Segment__c"/>
					</industrySegment_t>
					
					<industrySubSegment_t>
						<xsl:value-of select="//sf:Industry_Sub_Segment__c"/>
					</industrySubSegment_t>
					
				</xsl:if>
				
				<!-- Quote is created by Account/Lead/Case-->
				<xsl:if test="string-length(//sf:Account/sf:type) = 0 or //sf:type = 'Case' or //sf:type = 'Lead'">
					<currencyDropDown_t>
						<xsl:value-of select="//sf:Currency__c"/>
					</currencyDropDown_t>
				</xsl:if>
				
				<!-- Quote is created by Lead/Case -->
				<xsl:if test="string-length(//sf:Account_Name__r/sf:Name) &gt; 0">
					<customerCompanyName_t>
						<xsl:value-of select="//sf:Account_Name__r/sf:Name"/>
					</customerCompanyName_t>
					<customerAccount_t>
						<xsl:value-of select="//sf:Account_Name__r/sf:Name"/>
					</customerAccount_t>
					
					<accountName_t>
						<xsl:value-of select="//sf:Account_Name__r/sf:Name"/>
					</accountName_t>
					
					<billToCompanyName_t>
						<xsl:value-of select="//sf:Account_Name__r/sf:Name"/>
					</billToCompanyName_t>
					
					<billToCustomerAccount_t>
						<xsl:value-of select="//sf:Account_Name__r/sf:Name"/>
					</billToCustomerAccount_t>
				</xsl:if>
				
				<!-- Quote created from Account/Opportunity -->
				<xsl:if test="string-length(//sf:Account/sf:Name) &gt; 0">
					<accountName_t>
						<xsl:value-of select="//sf:Account/sf:Name"/>
					</accountName_t>
					
					<customerCompanyName_t>
						<xsl:value-of select="//sf:Account/sf:Name"/>
					</customerCompanyName_t>
					
					<billToCompanyName_t>
						<xsl:value-of select="//sf:Account/sf:Name"/>
					</billToCompanyName_t>
				
					<customerAccount_t>
						<xsl:value-of select="//sf:Account/sf:Name"/>
					</customerAccount_t>
				
					<billToCustomerAccount_t>
						<xsl:value-of select="//sf:Account/sf:Name"/>
					</billToCustomerAccount_t>
				</xsl:if>
				
				
				
				<localLanguageCustomerName_t>
					<xsl:value-of select="//sf:Alternate_Account_Name__c"/>
				</localLanguageCustomerName_t>
				<oCN_quote>
					<xsl:value-of select="//sf:OCN__c "/>
				</oCN_quote>
				<!-- No need to sync Primary Product in CPQ -->
				<!--<opportunityProduct>
					<xsl:value-of select="//sf:Primary_Product__c"/>
				</opportunityProduct> -->

				<!--<paymentTerm_quote>
					<xsl:value-of select="//sf:Payment_Terms__c"/>
				</paymentTerm_quote> -->
				
				<sFDCPaymentTerms_t>
					<xsl:value-of select="//sf:Payment_Terms__c"/>
				</sFDCPaymentTerms_t>

				<dongle_t>
					<xsl:value-of select="//sf:Dongle__c"/>
				</dongle_t>

				<hwUpgradeModelSN_t>
					<xsl:value-of select="//sf:Hardware_upgrade_Model_SN__c"/>
				</hwUpgradeModelSN_t>

				<customerCategory_t>
					<xsl:value-of select="//sf:Account_Class__c"/>
				</customerCategory_t>

				<salesChannel_t>
					<xsl:value-of select="//sf:Sales_Channel__c"/>
				</salesChannel_t>
				
				<eRPSalesChannel_t>
					<xsl:call-template name="salesChannelTransformation">
						<xsl:with-param name="sfdcSalesChannel" select="//sf:Sales_Channel__c"/>
					</xsl:call-template>
				</eRPSalesChannel_t>

				<customerScreeningInfo_t>
					<xsl:value-of select="//sf:Customer_Screening__c"/>
				</customerScreeningInfo_t>
				
				<endCustomerAccountId_t>
					<xsl:value-of select="//sf:End_Customer_Account__c"/>
				</endCustomerAccountId_t>
				
				<crmCustomerId_t>
					<xsl:choose>
						<xsl:when test="//sf:type = 'Case'">
							<xsl:value-of select="//sf:ContactId"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="//sf:Contact__c"/>
						</xsl:otherwise>
					</xsl:choose>
				</crmCustomerId_t>
				
				<incoterm_quote>
					<xsl:value-of select="//sf:Incoterm__c"/>
				</incoterm_quote>
				<opportunityAddress_t>
					<xsl:value-of select="//sf:Quote_To_Address__c"/>
				</opportunityAddress_t>
				
				<salesRepCode_quote>
					<xsl:value-of select="//sf:FE_Code__r/sf:Name"/>
				</salesRepCode_quote>
				
				<xsl:if test="string-length(//sf:FE_Code__r/sf:Name) = 0 and  //sf:type = 'Lead'">
					<leadFECodeFromAcct_t>true</leadFECodeFromAcct_t>
				</xsl:if>
                
                <districtManagerId_t>
					<xsl:value-of select="//sf:FE_Code__r/sf:District_Manager__c"/>
				</districtManagerId_t>
				
				<salesRepName_quote>
					<xsl:value-of select="//sf:FE_Code__r/sf:FE__r/sf:Name"/>
				</salesRepName_quote>
								
				<fERegion_t>
					<xsl:call-template name="regionTransformation">
						<xsl:with-param name="sfdcRegion" select="//sf:FE_Code__r/sf:Region__c"/>
					</xsl:call-template>
				</fERegion_t>
				
				<salesRepFirstName_t>
					<xsl:value-of select="//sf:FE__r/sf:FirstName"/>
				</salesRepFirstName_t>
				
				<salesRepLastName_t>
					<xsl:value-of select="//sf:FE__r/sf:LastName"/>
				</salesRepLastName_t>
				
				<nLSSalesRepFirstName_t>
					<xsl:value-of select="//sf:FE__r/sf:NLS_First_Name__c"/>
				</nLSSalesRepFirstName_t>
				
				<nLSSalesRepLastName_t>
					<xsl:value-of select="//sf:FE__r/sf:NLS_Last_Name__c"/>
				</nLSSalesRepLastName_t>
				
				<nLSSalesRepLanguageCode_t>
					<xsl:value-of select="//sf:FE__r/sf:NLS_Language_Code__c"/>
				</nLSSalesRepLanguageCode_t>
				
				<salesRepFormalName_t>
					<xsl:value-of select="//sf:FE__r/sf:Formal_Name__c"/>
				</salesRepFormalName_t>
				
				<salesRepTelephone_t>
					<xsl:value-of select="//sf:FE__r/sf:Phone"/>
				</salesRepTelephone_t>
				
				<salesRepEmail_t>
					<xsl:value-of select="//sf:FE__r/sf:Email"/>
				</salesRepEmail_t>
				
				<endCustomerName_t>
						<xsl:value-of select="//sf:End_Customer_Account__r/sf:Name"/>
				</endCustomerName_t>
				
				<endCustomerCompanyName_t>
						<xsl:value-of select="//sf:End_Customer_Account__r/sf:Name"/>
				</endCustomerCompanyName_t>
				
				<endCustomerAccount_t>
						<xsl:value-of select="//sf:End_Customer_Account__r/sf:Name"/>
				</endCustomerAccount_t>
				
				<dMCPQLogin_t>
					<xsl:value-of select="//sf:District_Manager__r/sf:BigMachines__Login__c"/>
				</dMCPQLogin_t>
				
				<fECPQLogin_t>
					<xsl:value-of select="//sf:FE__r/sf:BigMachines__Login__c"/>
				</fECPQLogin_t>
				
				<approvalRegion_t>
					<xsl:call-template name="approvalRegaionTransformation">
						<xsl:with-param name="sfdcRegion" select="//sf:FE_Code__r/sf:Region__c"/>
					</xsl:call-template>
				</approvalRegion_t>
				
			 </xsl:if>
			</document>
		</data_xml>
	</xsl:template>
	
	<xsl:template name="regionTransformation">
		<xsl:param name="sfdcRegion"/>
		<xsl:choose>
			<xsl:when test="$sfdcRegion = 'Region - America'">America</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - EMEAI'">Europe</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - GCR' or $sfdcRegion = 'Region - KSAP'">Asia Pacific</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - Japan'">Japan</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="approvalRegaionTransformation">
		<xsl:param name="sfdcRegion"/>
		<xsl:choose>
			<xsl:when test="$sfdcRegion = 'Region - America'">America</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - EMEAI'">Europe</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - GCR'">GCR</xsl:when> 
			<xsl:when test="$sfdcRegion = 'Region - KSAP'">Asia Pacific</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - Japan'">Japan</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<!--Defect# 1558, Updating eRPSalesChannel_t based upon provided mapping -->
	<xsl:template name = "salesChannelTransformation">
		<xsl:param name="sfdcSalesChannel"/>
		<xsl:choose>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_UNAUTHORIZED_RESELLER'">INDIRECT_UNAUTHORIZED_RESELLER</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'FTO'">FTO</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_SP'">INDIRECT_SP</xsl:when> 
			<xsl:when test="$sfdcSalesChannel = 'DIRECT_LEASING'">DIRECT_LEASING</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_SI_DIRECT'">INDIRECT_SI</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_VAR'">INDIRECT_VAR</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_VAR_DIRECT'">INDIRECT_VAR</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_SI'">INDIRECT_SI</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_OEM_DIRECT'">INDIRECT_OEM</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_OEM'">INDIRECT_OEM</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_IDR'">INDIRECT_IDR</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_DIST_MASTER'">INDIRECT_DIST_MASTER</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_ISS'">INDIRECT_ISS</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_DIST'">INDIRECT_DIST</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_ATP'">INDIRECT_ATP</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'DIRECT'">DIRECT</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_SBFR'">INDIRECT_SBFR</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_SP_DIRECT'">INDIRECT_SP</xsl:when>
			<xsl:when test="$sfdcSalesChannel = 'INDIRECT_OEM_DIRECT'">INDIRECT_OEM</xsl:when>
			
		</xsl:choose>
		
	</xsl:template>
	
</xsl:stylesheet>