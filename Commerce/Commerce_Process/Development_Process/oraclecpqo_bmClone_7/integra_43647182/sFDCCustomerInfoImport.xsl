<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com" version="1.0">
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:output method="xml"/>
	<xsl:template match="*">
		<data_xml>
			<document document_var_name="transaction">
					<customerContact_quote>
						<xsl:value-of select="//sf:Name"/>
					</customerContact_quote>
					
					<customerPhone_t>
						<xsl:value-of select="//sf:Phone"/>
					</customerPhone_t>
					
					<customerEmail_t>
						<xsl:value-of select="//sf:Email"/>
					</customerEmail_t>
					
					<customerContactFirstName_t>
						<xsl:value-of select="//sf:FirstName"/>
					</customerContactFirstName_t>
					
					<customerContactLastName_t>
						<xsl:value-of select="//sf:LastName"/>
					</customerContactLastName_t>
					
					<customerContactSalutation_t>
						<xsl:value-of select="//sf:Salutation"/>
					</customerContactSalutation_t>
					
					<customerContactAlternateFirstName_t>
						<xsl:value-of select="//sf:Alternate_First_Name__c"/>
					</customerContactAlternateFirstName_t>
					
					<customerContactAlternateLastName_t>
						<xsl:value-of select="//sf:Alternate_Last_Name__c"/>
					</customerContactAlternateLastName_t>
					
					<xsl:if test="not(//sf:Alternate_First_Name__c = '') and not(//sf:Alternate_Last_Name__c = '')">
						<customerContactAlternateSalutation_t>
							<xsl:value-of select="//sf:Salutation"/>
						</customerContactAlternateSalutation_t>
					</xsl:if>
					
					<department_t>
						<xsl:value-of select="//sf:Department"/>
					</department_t>
					
			</document>
		</data_xml>
	</xsl:template>
</xsl:stylesheet>