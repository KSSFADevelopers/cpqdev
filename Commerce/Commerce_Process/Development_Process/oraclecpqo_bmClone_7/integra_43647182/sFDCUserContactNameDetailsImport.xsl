<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com" version="1.0">
<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
<xsl:output method="xml"/>
<xsl:template match="*">
<data_xml>
<document document_var_name="transaction">
<cFDFEEmailSignatureName_t>
<xsl:value-of select="//sf:Name"/>
</cFDFEEmailSignatureName_t>
<cFDFEEmailSignatureFirstName_t>
<xsl:value-of select="//sf:FirstName"/>
</cFDFEEmailSignatureFirstName_t>
<cFDFEEmailSignatureLastName_t>
<xsl:value-of select="//sf:LastName"/>
</cFDFEEmailSignatureLastName_t>
<cFDFEEmailSignaturePhoneNumber_t>
<xsl:value-of select="//sf:Phone"/>
</cFDFEEmailSignaturePhoneNumber_t>
<cFDFEEmailSignatureNLSFirstName_t>
<xsl:value-of select="//sf:NLS_First_Name__c"/>
</cFDFEEmailSignatureNLSFirstName_t>
<cFDFEEmailSignatureNLSLastName_t>
<xsl:value-of select="//sf:NLS_Last_Name__c"/>
</cFDFEEmailSignatureNLSLastName_t>
<cFDFEEmailSignatureNLSLangCode_t>
<xsl:value-of select="//sf:NLS_Language_Code__c"/>
</cFDFEEmailSignatureNLSLangCode_t>
<cFDFEEmailSignatureFormalName_t>
<xsl:value-of select="//sf:Formal_Name__c"/>
</cFDFEEmailSignatureFormalName_t>
</document>
</data_xml>
</xsl:template>
</xsl:stylesheet>