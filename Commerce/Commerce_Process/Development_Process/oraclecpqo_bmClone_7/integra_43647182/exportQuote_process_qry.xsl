<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml"/>
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:variable name="bmTransId" select="/transaction/@id"/>
	<xsl:variable name="primaryProduct" select="/transaction/data_xml/document[@data_type=3]/_part_number[../_parent_doc_number != ''] [../bOMItemType='Model']"/>
	<xsl:variable name="parentDocNum" select="/transaction/data_xml/document[@data_type = 3]/_parent_doc_number[../_parent_doc_number != ''][../bOMItemType='Model']"/>
	
	<xsl:variable name = "status">
		<xsl:choose>
			<xsl:when test="$main_doc/status_t = 'pending'"> <!-- If status pending then need to get label name as "Draft" -->
				<xsl:value-of select="'Draft'"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$main_doc/status_t"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
			
	<xsl:strip-space elements="*"/>
	
	<!-- template: main -->
	<xsl:template match="/">
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<soap:Header>
				<SessionHeader xmlns="urn:partner.soap.sforce.com">
					<sessionId><xsl:value-of select="/transaction/user_info/session_id"/></sessionId>
				</SessionHeader>
				<CallOptions xmlns="urn:partner.soap.sforce.com">
					<client>BigMachinesLFE/1.0</client>
					<defaultNamespace>BigMachines</defaultNamespace>
				</CallOptions>
			</soap:Header>
			<soap:Body soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
				<upsert xmlns="urn:partner.soap.sforce.com">	
					<externalIDFieldName>BigMachines__Transaction_Id__c</externalIDFieldName>
					<xsl:call-template name="upsertQuote"/>
				</upsert>
			</soap:Body>
		</soap:Envelope>
	</xsl:template>

	<!-- template: upsertQuote - this template creates a new or modifies an existing quote in SFDC -->
	<xsl:template name="upsertQuote">
		<!-- ************************************ MODIFY START ************************************ -->
	<!-- If Oppty Stage is Booked then no need to synch Quote Information in SFA -->
	<xsl:if test = "$main_doc/sfaStage_t != 'Booked' or string-length($main_doc/sfaStage_t) = 0">
		<sObjects xmlns="urn:sobject.partner.soap.sforce.com">
			<type>BigMachines__Quote__c</type>
            <Site__c><xsl:value-of select="$main_doc/sFASiteId_t"/></Site__c>
			<Name><xsl:value-of select="$main_doc/transactionID_t"/></Name>
			<BigMachines__Transaction_Id__c><xsl:value-of select="$bmTransId"/></BigMachines__Transaction_Id__c>
			<xsl:if test="string-length($main_doc/opportunityID_t) = 15 or string-length($main_doc/opportunityID_t) = 18">
				<BigMachines__Opportunity__c><xsl:value-of select="$main_doc/opportunityID_t"/></BigMachines__Opportunity__c>
			</xsl:if>
			<BigMachines__Account__c><xsl:value-of select="$main_doc/_customer_id"/></BigMachines__Account__c>
			<BigMachines__Description__c><xsl:value-of select="$main_doc/transactionName_t"/></BigMachines__Description__c>
			<BigMachines__Status__c><xsl:value-of select="$status"/></BigMachines__Status__c>
			<Quote_Approval_Status__c><xsl:value-of select="$status"/></Quote_Approval_Status__c>
			<Total_Amount_Local__c><xsl:value-of select="$main_doc/totalOneTimeNetAmount_t"/></Total_Amount_Local__c>
			<BigMachines__Total__c><xsl:value-of select="$main_doc/totalOneTimeNetAmount_t"/></BigMachines__Total__c>
			<!-- Total Amount in USD -->
			<Total_Amount_USD__c>
				<xsl:choose>
					<xsl:when test="$main_doc/planRate_t != ''">
						<xsl:value-of select="format-number($main_doc/totalOneTimeNetAmount_t * number($main_doc/planRate_t),'0.00')"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</Total_Amount_USD__c>
			<BigMachines__Pricebook_Id__c><xsl:value-of select="$main_doc/_partner_price_book_id"/></BigMachines__Pricebook_Id__c>
			<BigMachines__Prep_Sync__c>true</BigMachines__Prep_Sync__c>
			<Revision__c><xsl:value-of select="$main_doc/version_number_versionTransaction_t"/></Revision__c>
			<QuotedCurrency__c><xsl:value-of select="$main_doc/currencyDropDown_t"/></QuotedCurrency__c>
			<Quote_Expiration_Date__c><xsl:value-of select="$main_doc/expirationDate_quote"/></Quote_Expiration_Date__c>
			
			<!--<currencyIsoCode><xsl:value-of select="$main_doc/currencyDropDown_t"/></currencyIsoCode>-->
			<xsl:choose>
				<!-- Defect# 5207 :: Lead -> Oppty Conversion no need to tag quote with Lead anymore. -->
				<xsl:when test="$main_doc/sfaObjectType_t = 'Lead' and string-length($main_doc/opportunityID_t) = 0">
					<Lead__c>
						<xsl:value-of select="$main_doc/sfaLead_t"/>
					</Lead__c>
				</xsl:when>
				<xsl:when test="$main_doc/sfaObjectType_t = 'Case'">
					<QOR__c>
						<xsl:value-of select="$main_doc/sfaCase_t"/>
					</QOR__c>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			
			<!-- Identify and Synch Primary Product -->
			<xsl:choose>
				<xsl:when test="$primaryProduct != ''">
					<Product_Name__c>
						<xsl:value-of select="$primaryProduct"/>
					</Product_Name__c>
					
					
				</xsl:when>
				<xsl:otherwise>
					 <fieldsToNull>Product_Name__c</fieldsToNull>
				</xsl:otherwise>
			</xsl:choose>
			
		<xsl:if test = "string-length($main_doc/regionalFileNetServerName_t) > 0">	
			<Filenet_region__c>
				<xsl:value-of select="$main_doc/regionalFileNetServerName_t"/>
			</Filenet_region__c>
		</xsl:if>	
		</sObjects>
	</xsl:if>
		<!-- ************************************ MODIFY END ************************************ -->
	</xsl:template>

</xsl:stylesheet>