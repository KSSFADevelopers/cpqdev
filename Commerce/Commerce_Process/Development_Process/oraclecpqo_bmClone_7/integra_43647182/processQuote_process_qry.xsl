<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>
	<xsl:output method="xml"/>
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:variable name="bmTransId" select="/transaction/@id"/>
	<xsl:strip-space elements="*"/>
	
	<!-- template: main -->
	<xsl:template match="/">
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<soap:Header>
				<SessionHeader xmlns="urn:partner.soap.sforce.com">
					<sessionId><xsl:value-of select="/transaction/user_info/session_id"/></sessionId>
				</SessionHeader>
				<CallOptions xmlns="urn:partner.soap.sforce.com">
					<client>BigMachinesLFE/1.0</client>
					<defaultNamespace>BigMachines</defaultNamespace>
				</CallOptions>
			</soap:Header>
			<soap:Body soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
				<xsl:choose>
					<xsl:when test="string-length($main_doc/sfaTransactionID_t) = 15 or string-length($main_doc/sfaTransactionID_t) = 18">
						<update xmlns="urn:partner.soap.sforce.com">
							<sObjects>
								<Id><xsl:value-of select="$main_doc/sfaTransactionID_t"/></Id>
								<type>Quote__c</type>
								<Process_Sync__c>true</Process_Sync__c>
							</sObjects>
						</update>
					</xsl:when>
					<xsl:otherwise>
						<_BM_IGNORE_PARTNER/>
					</xsl:otherwise>
				</xsl:choose>
			</soap:Body>
		</soap:Envelope>
	</xsl:template>
</xsl:stylesheet>