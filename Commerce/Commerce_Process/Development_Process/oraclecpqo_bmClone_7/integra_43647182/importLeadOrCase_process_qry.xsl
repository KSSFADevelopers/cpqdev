<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
	<xsl:output method="xml"/>
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:variable name="userId" select="/transaction/user_info/user_id"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
		<!-- Begin SOAP XML -->
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<soap:Header>
				<SessionHeader xmlns="urn:partner.soap.sforce.com">
					<sessionId><xsl:value-of select="/transaction/user_info/session_id"/></sessionId>
				</SessionHeader>
				<CallOptions xmlns="urn:partner.soap.sforce.com">
					<client>BigMachinesLFE/1.0</client>
				</CallOptions>
			</soap:Header>
			<soap:Body>
				<query xmlns="urn:partner.soap.sforce.com">
					<queryString>
						<xsl:choose>
							<!-- Quote created from Account/Lead/Case -->
							<xsl:when test='string-length($main_doc/opportunityID_t) = 0'>
								select Name, Object_Id__c, Object_Type__c, OwnerId from Account_CPQ_Quote_Map__c 
								where Account_Id__c='<xsl:value-of select="$main_doc/_customer_id"/>'  and OwnerId='<xsl:value-of select="$userId"/>'
							</xsl:when>
							<!-- Quote created from Opportunity -->
							<xsl:otherwise>
								Select Name from Account_CPQ_Quote_Map__c where Account_Id__c  = 'XXXXXXXXXXXXXXX'
							</xsl:otherwise>
						</xsl:choose>
					</queryString>
				</query>
			</soap:Body>
		</soap:Envelope>
		<!-- End SOAP XML -->
	</xsl:template>
</xsl:stylesheet>