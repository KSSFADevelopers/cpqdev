<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com" version="1.0">
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:output method="xml"/>
	<xsl:template match="*">
		<data_xml>
			<document document_var_name="transaction">
				<!--<endCustomerCompanyName_t>
					<xsl:value-of select="//sf:Name"/>
				
				</endCustomerCompanyName_t>-->
					
				<!--<endCustomerAccount_t>
					<xsl:value-of select="//sf:Name"/>
				</endCustomerAccount_t>-->
				
				<!--<endCustomerName_t>
					<xsl:value-of select="//sf:Name"/>
				</endCustomerName_t>-->
				
				<xsl:if test = "//sf:Primary__c = 'true'">
					<endCustomerAddress_t>
						<xsl:value-of select="//sf:Address_Line_1__c"/>
						<xsl:if test="string-length(//sf:Address_Line_2__c) &gt; 0">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="//sf:Address_Line_2__c"/>
						</xsl:if>
						<xsl:if test="string-length(//sf:Address_Line_3__c) &gt; 0">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="//sf:Address_Line_3__c"/>
						</xsl:if>
						<xsl:if test="string-length(//sf:Address_Line_4__c) &gt; 0">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="//sf:Address_Line_4__c"/>
						</xsl:if>
							
					</endCustomerAddress_t>
					
					<endCustomerAddress1_t>
						<xsl:value-of select="//sf:Address_Line_1__c"/>
					</endCustomerAddress1_t>
					
					<endCustomerAddress2_t>
						<xsl:value-of select="//sf:Address_Line_2__c"/>
					</endCustomerAddress2_t>
					<endCustomerAddress3_t>
						<xsl:value-of select="//sf:Address_Line_3__c"/>
					</endCustomerAddress3_t>
					
					<endCustomerAddress4_t>
						<xsl:value-of select="//sf:Address_Line_4__c"/>
					</endCustomerAddress4_t>
					
					<endCustomerCity_t>
						<xsl:value-of select="//sf:City__c"/>
					</endCustomerCity_t>
					
					<endCustomerCounty_t>
						<xsl:value-of select="//sf:County__c"/>
					</endCustomerCounty_t>
					
					<endCustomerState_t>
						<xsl:value-of select="//sf:StatePicklist__c"/>
					</endCustomerState_t>
					
					<endCustomerProvince_t>
						<xsl:value-of select="//sf:ProvincePicklist__c"/>
					</endCustomerProvince_t>
					
					<endCustomerZip_t>
						<xsl:value-of select="//sf:Postal_Code__c"/>
					</endCustomerZip_t>
					
					<endCustomerCountry_t>
						<!--<xsl:value-of select="//sf:GlobalCountry__c"/>-->
						<xsl:value-of select="//sf:Country_Code__c"/>
					</endCustomerCountry_t>
						
					<endCustomerAddressOSN_t>
						<xsl:value-of select="//sf:OSN__c"/>
					</endCustomerAddressOSN_t>
					
					<localAlternateAddressEnd_t>
						<xsl:value-of select="//sf:Alternate_Address_Line_1__c"/>
						<xsl:if test="string-length(//sf:Alternate_Address_Line_2__c) &gt; 0">
							<xsl:text>, </xsl:text>	
							<xsl:value-of select="//sf:Alternate_Address_Line_2__c"/>
						</xsl:if>
						<xsl:if test="string-length(//sf:Alternate_Address_Line_3__c) &gt; 0">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="//sf:Alternate_Address_Line_3__c"/>
						</xsl:if>
						<xsl:if test="string-length(//sf:Alternate_Address_Line_4__c) &gt; 0">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="//sf:Alternate_Address_Line_4__c"/>
						</xsl:if>
						<xsl:if test="string-length(//sf:Alternate_City__c) &gt; 0">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="//sf:Alternate_City__c"/>
						</xsl:if>
						<xsl:if test="string-length(//sf:Alternate_State__c) &gt; 0">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="//sf:Alternate_State__c"/>
						</xsl:if>
						<xsl:if test="string-length(//sf:Alternate_Province__c) &gt; 0">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="//sf:Alternate_Province__c"/>
						</xsl:if>
						<xsl:if test="string-length(//sf:Alternate_Postal_Code__c) &gt; 0">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="//sf:Alternate_Postal_Code__c"/>
						</xsl:if>
						<xsl:if test="string-length(//sf:Alternate_Country__c) &gt; 0">
							<xsl:text>, </xsl:text>
							<xsl:value-of select="//sf:Alternate_Country__c"/>
						</xsl:if>
					</localAlternateAddressEnd_t>
				</xsl:if>
				
			</document>
		</data_xml>
	</xsl:template>
</xsl:stylesheet>