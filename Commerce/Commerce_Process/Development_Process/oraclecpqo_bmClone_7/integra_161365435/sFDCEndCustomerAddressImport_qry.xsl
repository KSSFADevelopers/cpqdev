<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version='1.0'>
	<xsl:output method="xml"/>
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
		<!-- Begin SOAP XML -->
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<soap:Header>
				<SessionHeader xmlns="urn:partner.soap.sforce.com">
					<sessionId><xsl:value-of select="/transaction/user_info/session_id"/></sessionId>
				</SessionHeader>
				<CallOptions xmlns="urn:partner.soap.sforce.com">
					<client>BigMachinesLFE/1.0</client>
				</CallOptions>
			</soap:Header>
			<soap:Body>
				<query xmlns="urn:partner.soap.sforce.com">
					<queryString>
						<xsl:choose>
							<xsl:when test="string-length($main_doc/endCustomerAccountId_t) > 0">
								Select Address_Line_1__c,Address_Line_2__c, Address_Line_3__c,Address_Line_4__c,City__c,County__c ,StatePicklist__c, ProvincePicklist__c,Postal_Code__c, GlobalCountry__c,OSN__c,Primary__c,
								Alternate_Address_Line_1__c,Alternate_Address_Line_2__c, Alternate_Address_Line_3__c, Alternate_Address_Line_4__c,Alternate_City__c,
								Alternate_State__c, Alternate_County__c, Alternate_Province__c, Alternate_Postal_Code__c, Alternate_Country__c,Country_Code__c, 
								Associated_account__r.Name
								from Address__c where Associated_account__r.Id = '<xsl:value-of select = "$main_doc/endCustomerAccountId_t"/>'
							</xsl:when>
							<xsl:otherwise>
								Select Id from Address__c where Id = 'XXXXXXXXXXXXXXX'
							</xsl:otherwise>
						</xsl:choose>
					</queryString>
				</query>
			</soap:Body>
		</soap:Envelope>
		<!-- End SOAP XML -->
	</xsl:template>
</xsl:stylesheet>
