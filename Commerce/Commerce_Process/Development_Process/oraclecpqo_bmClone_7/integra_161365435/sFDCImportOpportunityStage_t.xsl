<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com" version="1.0">
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:output method="xml"/>
	<xsl:template match="*">
		<data_xml>
			<xsl:if test = "//sf2:size > 0">
				<document document_var_name="transaction"> 
					<sfaStage_t>
						<xsl:value-of select="//sf:StageName"/>
					</sfaStage_t>
					
					<industrySegment_t>
						<xsl:value-of select="//sf:Industry_Segment__c"/>
					</industrySegment_t>
					
					<industrySubSegment_t>
						<xsl:value-of select="//sf:Industry_Sub_Segment__c"/>
					</industrySubSegment_t>
					
					<xsl:if test="string-length(//sf:Purchase_Order__c) &gt; 0">
						<purchaseOrderNumber_t>
							<xsl:value-of select="//sf:Purchase_Order__c"/>
						</purchaseOrderNumber_t>
					</xsl:if>
					
				</document>
			</xsl:if>
		</data_xml>
	</xsl:template>
</xsl:stylesheet>