<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com" version="1.0">
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:output method="xml"/>
	<xsl:template match="*">
		<data_xml>
			<document document_var_name="transaction">
			 <xsl:if test="//sf2:size &gt; 0">
				<salesRepCode_quote>
					<xsl:value-of select="//sf:FE_Code__r/sf:Name"/>
				</salesRepCode_quote>
				
				<salesRepName_quote>
					<xsl:value-of select="//sf:FE_Code__r/sf:FE__r/sf:Name"/>
				</salesRepName_quote>
								
				<fERegion_t>
					<xsl:call-template name="regionTransformation">
						<xsl:with-param name="sfdcRegion" select="//sf:FE_Code__r/sf:Region__c"/>
					</xsl:call-template>
				</fERegion_t>
				
				<salesRepFirstName_t>
					<xsl:value-of select="//sf:FE__r/sf:FirstName"/>
				</salesRepFirstName_t>
				
				<salesRepLastName_t>
					<xsl:value-of select="//sf:FE__r/sf:LastName"/>
				</salesRepLastName_t>
				
				<nLSSalesRepFirstName_t>
					<xsl:value-of select="//sf:FE__r/sf:NLS_First_Name__c"/>
				</nLSSalesRepFirstName_t>
				
				<nLSSalesRepLastName_t>
					<xsl:value-of select="//sf:FE__r/sf:NLS_Last_Name__c"/>
				</nLSSalesRepLastName_t>
				
				<nLSSalesRepLanguageCode_t>
					<xsl:value-of select="//sf:FE__r/sf:NLS_Language_Code__c"/>
				</nLSSalesRepLanguageCode_t>
				
				<salesRepFormalName_t>
					<xsl:value-of select="//sf:FE__r/sf:Formal_Name__c"/>
				</salesRepFormalName_t>
				
				<salesRepTelephone_t>
					<xsl:value-of select="//sf:FE__r/sf:Phone"/>
				</salesRepTelephone_t>
				
				<salesRepEmail_t>
					<xsl:value-of select="//sf:FE__r/sf:Email"/>
				</salesRepEmail_t>
				
				
				<dMCPQLogin_t>
					<xsl:value-of select="//sf:District_Manager__r/sf:BigMachines__Login__c"/>
				</dMCPQLogin_t>
				
				<fECPQLogin_t>
					<xsl:value-of select="//sf:FE__r/sf:BigMachines__Login__c"/>
				</fECPQLogin_t>
				
				<approvalRegion_t>
					<xsl:call-template name="approvalRegaionTransformation">
						<xsl:with-param name="sfdcRegion" select="//sf:FE_Code__r/sf:Region__c"/>
					</xsl:call-template>
				</approvalRegion_t>
			 </xsl:if>
			</document>
		</data_xml>
	</xsl:template>
	
	<xsl:template name="regionTransformation">
		<xsl:param name="sfdcRegion"/>
		<xsl:choose>
			<xsl:when test="$sfdcRegion = 'Region - America'">America</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - EMEAI'">Europe</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - GCR' or $sfdcRegion = 'Region - KSAP'">Asia Pacific</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - Japan'">Japan</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="approvalRegaionTransformation">
		<xsl:param name="sfdcRegion"/>
		<xsl:choose>
			<xsl:when test="$sfdcRegion = 'Region - America'">America</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - EMEAI'">Europe</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - GCR'">GCR</xsl:when> 
			<xsl:when test="$sfdcRegion = 'Region - KSAP'">Asia Pacific</xsl:when>
			<xsl:when test="$sfdcRegion = 'Region - Japan'">Japan</xsl:when>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>