<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com">
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:output method="xml"/>

	<xsl:template match="*">
		<data_xml>
			<xsl:if test="string-length(//sf:Object_Id__c) &gt; 0 and //sf2:size > 0">
				<document document_var_name="transaction">
					<xsl:variable name="objectType" select="//sf:Object_Type__c"/>
					<sfaObjectType_t><xsl:value-of select="$objectType"/></sfaObjectType_t>
					<xsl:choose>
						<xsl:when test="$objectType = 'Case'">
							<sfaCase_t><xsl:value-of select="//sf:Object_Id__c"/></sfaCase_t>
						</xsl:when>
						<xsl:when test="$objectType = 'Lead'">
							<sfaLead_t><xsl:value-of select="//sf:Object_Id__c"/></sfaLead_t>
						</xsl:when>
						<xsl:otherwise></xsl:otherwise>
					</xsl:choose>
				</document>
			</xsl:if>
		</data_xml>
	</xsl:template>

</xsl:stylesheet>
