<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version='1.0'>
	<xsl:output method="xml"/>
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
		<!-- Begin SOAP XML -->
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<soap:Header>
				<SessionHeader xmlns="urn:partner.soap.sforce.com">
					<sessionId><xsl:value-of select="/transaction/user_info/session_id"/></sessionId>
				</SessionHeader>
				<CallOptions xmlns="urn:partner.soap.sforce.com">
					<client>BigMachinesLFE/1.0</client>
				</CallOptions>
			</soap:Header>
			<soap:Body>
				<!-- In case of Quote to Address Change FE related Info and Currency info needs to fetch from SFDC -->
				<query xmlns="urn:partner.soap.sforce.com">
					<queryString>
						<xsl:choose>
							<xsl:when test="string-length($main_doc/tempOCN_t) > 0 and $main_doc/tempOCN_t != $main_doc/oCN_quote and string-length($main_doc/opportunityID_t) > 0 and $main_doc/addressType_quote = 'quoteToAddress'">
								SELECT Quote_Currency__c,
								FE_Code__r.Name,FE_Code__r.Region__c,FE_Code__r.FE__r.Name, FE_Code__r.FE__r.FirstName,FE_Code__r.FE__r.LastName,FE_Code__r.FE__r.Phone, FE_Code__r.FE__r.Email, FE_Code__r.FE__r.NLS_First_Name__c , FE_Code__r.FE__r.NLS_Last_Name__c, FE_Code__r.FE__r.NLS_Language_Code__c, FE_Code__r.FE__r.Formal_Name__c,FE_Code__r.FE__r.BigMachines__Login__c,
								FE_Code__r.District_Manager__r.BigMachines__Login__c
								from Opportunity where Id= '<xsl:value-of select="$main_doc/opportunityID_t" />' 
							</xsl:when>
							<xsl:otherwise>
								Select Name from Opportunity where Id = 'XXXXXXXXXXXXXXXXXX'
							</xsl:otherwise>
						</xsl:choose> 
					</queryString>
				</query>
			</soap:Body>
		</soap:Envelope>
		<!-- End SOAP XML -->
	</xsl:template>
</xsl:stylesheet>
