<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<!-- PLEASE ONLY MODIFY THE XSL WITHIN THE SECTION MARKED MODIFY -->
	<!-- BigMachines' Customer Service is only responsible for issues resulting from modifications within this section -->
	<xsl:output method="xml"/>
	<!-- data_type=0 : quote-level node -->
	<!-- data_type=2 : model-level node -->
	<!-- data_type=3 : line item node   -->
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:variable name="startPos" select="800"/>
	<xsl:variable name="endPos" select="1000"/>
	<xsl:strip-space elements="*"/>

	<!-- Main Template -->
	<xsl:template match="/">
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<soap:Header>
				<SessionHeader xmlns="urn:partner.soap.sforce.com">
					<sessionId><xsl:value-of select="/transaction/user_info/session_id"/></sessionId>
				</SessionHeader>
				<CallOptions xmlns="urn:partner.soap.sforce.com">
					<client>BigMachinesLFE/1.0</client>
					<defaultNamespace>BigMachines</defaultNamespace>
				</CallOptions>
			</soap:Header>
			<soap:Body soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
				<upsert xmlns="urn:partner.soap.sforce.com">
					<ExternalIDFieldName>BigMachines__External_Id__c</ExternalIDFieldName>
					<xsl:choose>
						<xsl:when test="(string-length($main_doc/sfaTransactionID_t) = 15 or string-length($main_doc/sfaTransactionID_t) = 18) 
										and count(/transaction/data_xml/document[@data_type=3]) &gt; $startPos and count(/transaction/data_xml/document[@data_type=3]) &lt;= 1000">
							<xsl:call-template name="upsertLines"/>
						</xsl:when>
						<xsl:otherwise><_BM_IGNORE_PARTNER/></xsl:otherwise>	
					</xsl:choose>
				</upsert>
			</soap:Body>
		</soap:Envelope>
	</xsl:template>

	<!-- template: upsertLines - this template creates new or modifies existing line items in SFDC -->
	<xsl:template name="upsertLines">
		<xsl:for-each select="/transaction/data_xml/document[@data_type=3][position() &gt; $startPos and position() &lt;= $endPos]">
			<xsl:sort select="_sequence_number" data-type="number"/>
			<xsl:variable name="sub_doc" select="."/>
			<!-- ************************************ MODIFY START ************************************ -->
		<!-- If Oppty Stage is Booked then no need to synch Product Information in SFA -->
		<xsl:if test = "$main_doc/sfaStage_t != 'Booked' or string-length($main_doc/sfaStage_t) = 0">
			<xsl:if test="(string-length($sub_doc/_parent_doc_number) = 0  or $sub_doc/bOMItemType = 'Model' ) and not(string-length($sub_doc/_part_number) = 0) ">
			<!-- Only Top Model needs to sync back OR Stand Alone Parts-->
				<xsl:variable name="name">
					<xsl:choose>
						<xsl:when test="string-length($sub_doc/_part_number) > 1"><xsl:value-of select="$sub_doc/_part_number"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="$sub_doc/_model_name"/></xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="desc">
					<xsl:choose>
						<xsl:when test="string-length($sub_doc/_part_desc) > 1"><xsl:value-of select="$sub_doc/_part_desc"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="$sub_doc/_model_product_line_name"/></xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="netPrice">
					<!-- hi-tech uses contractValue_l instead of netPriceEach_line -->
					<xsl:choose>
						<xsl:when test="$sub_doc/netPriceSFA_l != ''">
							<xsl:value-of select="$sub_doc/netPriceSFA_l"/>
						</xsl:when> 
						
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				
				<xsl:variable name = "netAmount">
					<xsl:choose>
						<xsl:when test="$sub_doc/netAmountSFA_l != ''">
							<xsl:value-of select="$sub_doc/netAmountSFA_l"/>
						</xsl:when> 
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				
				<sObjects xmlns="urn:sobject.partner.soap.sforce.com">
					<type>Quote_Product__c</type>
					<External_Id__c><xsl:value-of select="$sub_doc/sfaExternalID_l"/></External_Id__c>
					<Quote__c><xsl:value-of select="$main_doc/sfaTransactionID_t"/></Quote__c>
					<Name><xsl:value-of select="$name"/></Name>
					<Description__c><xsl:value-of select="$desc"/></Description__c>
					<Quantity__c><xsl:value-of select="$sub_doc/_price_quantity"/></Quantity__c>
					<Sales_Price__c><xsl:value-of select="$netPrice"/></Sales_Price__c>
					
					<!-- Sales Price conversion from Local currency to USD Amount -->
					<Sales_Price_USD__c>
						<xsl:choose>
							<xsl:when test="$main_doc/planRate_t != ''">
								<xsl:value-of select="format-number($netPrice * number($main_doc/planRate_t),'0.00')"/>
							</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</Sales_Price_USD__c>
					
					
					<Total_Amount_Local__c><xsl:value-of select="$netAmount"/></Total_Amount_Local__c>
					
					<!-- Net Amount Conversion from Local currency to USD Amount-->
					<Total_Amount_USD__c>
						<xsl:choose>
							<xsl:when test="$main_doc/planRate_t != ''">
								<xsl:value-of select="format-number($netAmount * number($main_doc/planRate_t),'0.00')"/>
							</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</Total_Amount_USD__c>
					
					<Prep_Delete__c>false</Prep_Delete__c>
					<Synchronization_Id__c><xsl:value-of select="$sub_doc/sfaSynchronizationID_l"/></Synchronization_Id__c>
					<Local_Currency_Code__c><xsl:value-of select="$main_doc/currencyDropDown_t"/></Local_Currency_Code__c>
					<!-- This section is used for renewals
						1. Create reciprocating fields on Quote Product Objects in SFDC
						2. Uncomment this section and update variable needs if needed.
					<Contract_Start_Date__c><xsl:value-of select="$sub_doc/contractStartDate_l"/></Contract_Start_Date__c>
					<Contract_End_Date__c><xsl:value-of select="$sub_doc/contractEndDate_l"/></Contract_End_Date__c>
						3. Use this asset ID if needed
					<Asset_Id__c><xsl:value-of select="$sub_doc/assetID_l"/></Asset_Id__c>
					-->
				</sObjects>
			</xsl:if> 
		</xsl:if> 
			<!-- ************************************ MODIFY END ************************************ -->
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>