<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>
	<xsl:output method="xml"/>
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:variable name="bmTransId" select="/transaction/@id"/>
	<xsl:strip-space elements="*"/>
	
	<!-- template: main -->
	<xsl:template match="/">
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<soap:Header>
				<SessionHeader xmlns="urn:partner.soap.sforce.com">
					<sessionId><xsl:value-of select="/transaction/user_info/session_id"/></sessionId>
				</SessionHeader>
				<CallOptions xmlns="urn:partner.soap.sforce.com">
					<client>BigMachinesLFE/1.0</client>
					<defaultNamespace>BigMachines</defaultNamespace>
				</CallOptions>
			</soap:Header>
			<soap:Body soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
				<upsert xmlns="urn:partner.soap.sforce.com">	
					<externalIDFieldName>Opportunity_Id__c</externalIDFieldName>
					
					<xsl:if test = "$main_doc/primaryQuote_t = 'true'">
						<xsl:call-template name="upsertOpportunity"/>
					</xsl:if>
				</upsert>
			</soap:Body>
		</soap:Envelope>
	</xsl:template>

	<!-- template: upsertQuote - this template creates a new or modifies an existing Opportunity in SFDC -->
	<xsl:template name="upsertOpportunity">
		<!-- ************************************ MODIFY START ************************************ -->
		<sObjects xmlns="urn:sobject.partner.soap.sforce.com">
			<type>Opportunity</type>
			<Opportunity_Id__c><xsl:value-of select="substring($main_doc/opportunityID_t,1,15)"/></Opportunity_Id__c>
			
			<xsl:if test="$main_doc/purchaseOrderNumber_t != ''">
				<Purchase_Order__c><xsl:value-of select="$main_doc/purchaseOrderNumber_t"/></Purchase_Order__c>
			</xsl:if>
		</sObjects>
		<!-- ************************************ MODIFY END ************************************ -->
	</xsl:template>
</xsl:stylesheet>