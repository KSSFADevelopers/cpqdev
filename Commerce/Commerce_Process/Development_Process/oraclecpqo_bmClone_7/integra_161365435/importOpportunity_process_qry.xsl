<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
	<xsl:output method="xml"/>
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
		<!-- Begin SOAP XML -->
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<soap:Header>
				<SessionHeader xmlns="urn:partner.soap.sforce.com">
					<sessionId><xsl:value-of select="/transaction/user_info/session_id"/></sessionId>
				</SessionHeader>
				<CallOptions xmlns="urn:partner.soap.sforce.com">
					<client>BigMachinesLFE/1.0</client>
				</CallOptions>
			</soap:Header>
			<soap:Body>
				<query xmlns="urn:partner.soap.sforce.com">
					<queryString>
						<xsl:choose>
							<xsl:when test="$main_doc/copyQuote_t = 'false'"> <!-- On Cloned Quote no need to run Integration -->
								<xsl:choose>
									<xsl:when test="string-length($main_doc/opportunityID_t) &gt; 0">
										Select Name,CloseDate,StageName,Industry_Segment__c,Industry_Sub_Segment__c,
										Account.Sales_Channel__c, Account.Name, Account.Alternate_Account_Name__c, Account.OCN__c, Account.Integration_ID__c,Account.Payment_Terms__c, Account.Account_Class__c, Account.Customer_Screening__c,Account.Incoterm__c,
										Quote_Currency__c, Dongle__c, Hardware_upgrade_Model_SN__c, End_Customer_Account__c,Contact__c,Quote_To_Address__c,
										End_Customer_Account__r.Name, 
										FE_Code__r.Name,FE_Code__r.Region__c,FE_Code__r.FE__r.Name, FE_Code__r.FE__r.FirstName,FE_Code__r.FE__r.LastName,FE_Code__r.FE__r.Phone, FE_Code__r.FE__r.Email, FE_Code__r.FE__r.NLS_First_Name__c , FE_Code__r.FE__r.NLS_Last_Name__c, FE_Code__r.FE__r.NLS_Language_Code__c, FE_Code__r.FE__r.Formal_Name__c,FE_Code__r.FE__r.BigMachines__Login__c,
										FE_Code__r.District_Manager__r.BigMachines__Login__c
										from Opportunity where Id= '<xsl:value-of select="$main_doc/opportunityID_t"/>' 
									</xsl:when>
									<xsl:otherwise> <!-- Quote created by Lead -->
									   <xsl:choose>
											<xsl:when test="$main_doc/sfaObjectType_t = 'Lead'"> <!--Quote is created using Lead-->
												Select Account_Name__r.Name,Account_Name__r.Alternate_Account_Name__c,Account_Name__r.OCN__c,  Account_Name__r.Integration_ID__c,Account_Name__r.Payment_Terms__c, Account_Name__r.Account_Class__c, Account_Name__r.Customer_Screening__c, Account_Name__r.Sales_Channel__c, Account_Name__r.Incoterm__c, Contact__c, Currency__c,
												FE_Code__r.FE__r.Name, FE_Code__r.Name, FE_Code__r.FE__r.FirstName, FE_Code__r.FE__r.LastName, FE_Code__r.FE__r.Phone, FE_Code__r.FE__r.Email, FE_Code__r.Region__c,FE_Code__r.FE__r.NLS_First_Name__c ,FE_Code__r.FE__r.NLS_Last_Name__c, FE_Code__r.FE__r.NLS_Language_Code__c, FE_Code__r.FE__r.Formal_Name__c,
												FE_Code__r.FE__r.BigMachines__Login__c,
												FE_Code__r.District_Manager__r.BigMachines__Login__c
												from Lead where Id= '<xsl:value-of select="$main_doc/sfaLead_t"/>'
											</xsl:when>
											<xsl:when test="$main_doc/sfaObjectType_t = 'Account'"> <!-- Quote Created by Account -->
												Select Name,Alternate_Account_Name__c,OCN__c,Integration_ID__c,Payment_Terms__c, Account_Class__c, Customer_Screening__c,Sales_Channel__c, Incoterm__c, Currency__c, FE_Code__r.FE__r.Name,FE_Code__r.Name,FE_Code__r.Region__c,FE_Code__r.FE__r.FirstName, FE_Code__r.FE__r.LastName, FE_Code__r.FE__r.Phone, FE_Code__r.FE__r.Email,FE_Code__r.FE__r.NLS_First_Name__c ,FE_Code__r.FE__r.NLS_Last_Name__c, FE_Code__r.FE__r.NLS_Language_Code__c, FE_Code__r.FE__r.Formal_Name__c,
												FE_Code__r.FE__r.BigMachines__Login__c,
												FE_Code__r.District_Manager__r.BigMachines__Login__c
												from Account where Id= '<xsl:value-of select="$main_doc/_customer_id"/>'
											</xsl:when>
											<xsl:when test="$main_doc/sfaObjectType_t = 'Case'"> <!-- Quote Created by Case -->
												Select Account.Name,Account.Alternate_Account_Name__c,Account.OCN__c,  Account.Integration_ID__c,Account.Payment_Terms__c, Account.Account_Class__c, Account.Customer_Screening__c, Account.Sales_Channel__c, Account.Incoterm__c, Account.Currency__c, 
												ContactId,
												Account.FE_Code__r.FE__r.Name, Account.FE_Code__r.Name, Account.FE_Code__r.Region__c, Account.FE_Code__r.FE__r.FirstName, Account.FE_Code__r.FE__r.LastName, Account.FE_Code__r.FE__r.Phone, Account.FE_Code__r.FE__r.Email,Account.FE_Code__r.FE__r.NLS_First_Name__c , Account.FE_Code__r.FE__r.NLS_Last_Name__c, Account.FE_Code__r.FE__r.NLS_Language_Code__c, Account.FE_Code__r.FE__r.Formal_Name__c, Account.FE_Code__r.FE__r.BigMachines__Login__c,
												Account.FE_Code__r.District_Manager__r.BigMachines__Login__c
												from Case where Id= '<xsl:value-of select="$main_doc/sfaCase_t"/>'
											</xsl:when>
											<xsl:otherwise>
												Select Name from Opportunity where Id = 'XXXXXXXXXXXXXXXXXX'
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise> <!-- othrwise for $main_doc/copyQuote_t = 'false' -->
								Select Name from Opportunity where Id = 'XXXXXXXXXXXXXXXXXX'
							</xsl:otherwise>
						</xsl:choose>
					</queryString>
				</query>
			</soap:Body>
		</soap:Envelope>
		<!-- End SOAP XML -->
	</xsl:template>
</xsl:stylesheet>