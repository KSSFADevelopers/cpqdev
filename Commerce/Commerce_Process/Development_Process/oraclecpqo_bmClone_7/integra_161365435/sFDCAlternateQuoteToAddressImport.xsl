<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com" version="1.0">
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:output method="xml"/>
	<xsl:template match="*">
		<data_xml>
			<document document_var_name="transaction">
			<xsl:if test = "//sf2:size > 0">
				<xsl:for-each select="//sf2:records">
						
						<xsl:if test="boolean(./sf:Industry_Segment__c)">
							<industrySegment_t>
								<xsl:value-of select="./sf:Industry_Segment__c"/>
							</industrySegment_t>
						</xsl:if>
						
						<xsl:if test="boolean(./sf:Industry_Sub_Segment__c)">
							<industrySubSegment_t>
								<xsl:value-of select="./sf:Industry_Sub_Segment__c"/>
							</industrySubSegment_t>
						</xsl:if>
						
						<quoteToAddress_quote>
							<xsl:value-of select="./sf:Address_Line_1__c"/>
							<xsl:if test="string-length(./sf:Address_Line_2__c) &gt; 0">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="./sf:Address_Line_2__c" />
							</xsl:if>
							<xsl:if test="string-length(./sf:Address_Line_3__c) &gt; 0">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="./sf:Address_Line_3__c"/>
							</xsl:if>
							<xsl:if test="string-length(./sf:Address_Line_4__c) &gt; 0">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="./sf:Address_Line_4__c"/>
							</xsl:if>
							<xsl:if test="string-length(./sf:City__c) &gt; 0">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="./sf:City__c"/>
							</xsl:if>
							
							<xsl:if test="string-length(./sf:County__c) &gt; 0">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="./sf:County__c"/>
							</xsl:if>
							<xsl:if test="string-length(./sf:StatePicklist__c) &gt; 0">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="./sf:StatePicklist__c"/>
							</xsl:if>
							<xsl:if test="string-length(./sf:ProvincePicklist__c) &gt; 0">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="./sf:ProvincePicklist__c"/>
							</xsl:if>
							<xsl:if test="string-length(./sf:Postal_Code__c) &gt; 0">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="./sf:Postal_Code__c"/>
							</xsl:if>
							<xsl:if test="string-length(./sf:Country_Code__c) &gt; 0">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="./sf:Country_Code__c"/>
							</xsl:if>
						</quoteToAddress_quote>
						
						<localAlternateAddress_t>
								<xsl:value-of select="./sf:Alternate_Address_Line_1__c"/>
								<xsl:if test="string-length(./sf:Alternate_Address_Line_2__c) &gt; 0">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="./sf:Alternate_Address_Line_2__c"/>
								</xsl:if>
								<xsl:if test="string-length(./sf:Alternate_Address_Line_3__c) &gt; 0">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="./sf:Alternate_Address_Line_3__c"/>
								</xsl:if>
								<xsl:if test="string-length(./sf:Alternate_Address_Line_4__c) &gt; 0">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="./sf:Alternate_Address_Line_4__c"/>
								</xsl:if>
								<xsl:if test="string-length(./sf:Alternate_City__c) &gt; 0">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="./sf:Alternate_City__c"/>
								</xsl:if>
								
								<xsl:if test="string-length(./sf:Alternate_County__c) &gt; 0">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="./sf:Alternate_County__c"/>
								</xsl:if>
								
								<xsl:if test="string-length(./sf:Alternate_State__c) &gt; 0">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="./sf:Alternate_State__c"/>
								</xsl:if>
								<xsl:if test="string-length(./sf:Alternate_Province__c) &gt; 0">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="./sf:Alternate_Province__c"/>
								</xsl:if>	
								<xsl:if test="string-length(./sf:Alternate_Postal_Code__c) &gt; 0">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="./sf:Alternate_Postal_Code__c"/>
								</xsl:if>
								<xsl:if test="string-length(./sf:Alternate_Country__c) &gt; 0">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="./sf:Alternate_Country__c"/>
								</xsl:if>
						</localAlternateAddress_t>
						
						<quoteToAddress1_quote>
							<xsl:value-of select="./sf:Address_Line_1__c"/>
						</quoteToAddress1_quote>
						
						<quoteToAddress2_quote>
							<xsl:value-of select="./sf:Address_Line_2__c"/>
						</quoteToAddress2_quote>
						<quoteToAddress3_quote>
							<xsl:value-of select="./sf:Address_Line_3__c"/>
						</quoteToAddress3_quote>
						
						<quoteToAddress4_quote>
							<xsl:value-of select="./sf:Address_Line_4__c"/>
						</quoteToAddress4_quote>
						
						<quoteToCity_quote>
							<xsl:value-of select="./sf:City__c"/>
						</quoteToCity_quote>
						
						<quoteToCounty_quote>
							<xsl:value-of select="./sf:County__c"/>
						</quoteToCounty_quote>
						
						<quoteToState_quote>
							<xsl:value-of select="./sf:StatePicklist__c"/>
						</quoteToState_quote>
						
						<quoteToProvince_quote>
							<xsl:value-of select="./sf:ProvincePicklist__c"/>
						</quoteToProvince_quote>
						
						<quoteToPostcode_quote>
							<xsl:value-of select="./sf:Postal_Code__c"/>
						</quoteToPostcode_quote>
						
						<quoteToCountry_quote>
							<xsl:value-of select="./sf:Country_Code__c"/>
						</quoteToCountry_quote>
						
						<endUseCountry>
							<xsl:value-of select="./sf:Country_Code__c"/>
						</endUseCountry>
						
						<shipToCountryAddress_t>
							<xsl:value-of select="./sf:Country_Code__c"/>
						</shipToCountryAddress_t>
							
						<quoteToAddressOSN_t>
							<xsl:value-of select="./sf:OSN__c"/>
						</quoteToAddressOSN_t>
						
				<!--Set Local Language Address Data -->
						<localLanguageAlternateAddress1_t>
							<xsl:value-of select="./sf:Alternate_Address_Line_1__c"/>
						</localLanguageAlternateAddress1_t>
						
						<localLanguageAlternateAddress2_t>
							<xsl:value-of select="./sf:Alternate_Address_Line_2__c"/>
						</localLanguageAlternateAddress2_t>
						
						<localLanguageAlternateAddress3_t>
							<xsl:value-of select="./sf:Alternate_Address_Line_3__c"/>
						</localLanguageAlternateAddress3_t>
						
						<localLanguageAlternateAddress4_t>
							<xsl:value-of select="./sf:Alternate_Address_Line_4__c"/>
						</localLanguageAlternateAddress4_t>
						
						<localLanguageAlternateCity_t>
							<xsl:value-of select="./sf:Alternate_City__c"/>
						</localLanguageAlternateCity_t>
						
						<localLanguageAlternateState_t>
							<xsl:value-of select="./sf:Alternate_State__c"/>
						</localLanguageAlternateState_t>
						
						<localLanguageAlternateCounty_t>
							<xsl:value-of select="./sf:Alternate_County__c"/>
						</localLanguageAlternateCounty_t>
						
						<localLanguageAlternateProvince_t>
							<xsl:value-of select="./sf:Alternate_Province__c"/>
						</localLanguageAlternateProvince_t>
						
						<localLanguageAlternatePostCode_t>
							<xsl:value-of select="./sf:Alternate_Postal_Code__c"/>
						</localLanguageAlternatePostCode_t>
						
						<localLanguageAlternateCountry_t>
							<xsl:value-of select="./sf:Alternate_Country__c"/>
						</localLanguageAlternateCountry_t>
						
						<localLanguageAddressLanguageIndicator_t>
							<xsl:value-of select="./sf:Alternate_Language_Indicator__c"/>
						</localLanguageAddressLanguageIndicator_t>
				</xsl:for-each>
			</xsl:if>
			</document>
		</data_xml>
	</xsl:template>
</xsl:stylesheet>