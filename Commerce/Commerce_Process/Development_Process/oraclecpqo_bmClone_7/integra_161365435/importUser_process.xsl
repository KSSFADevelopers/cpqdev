<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com">
	<xsl:output method="xml"/> 
	<xsl:template match="*">
		<data_xml>
			<xsl:if test="string-length(//sf:Id) > 0">
				<document document_var_name="transaction">
					<sfaUserID_t><xsl:value-of select="//sf:Id" /></sfaUserID_t>
					<proposalPreparedByName_t><xsl:value-of select="//sf:FirstName" />&#160;<xsl:value-of select="//sf:LastName" /></proposalPreparedByName_t>
					<proposalPreparedByTitle_t><xsl:value-of select="//sf:Title" /></proposalPreparedByTitle_t>
					<proposalPreparedByPhone_t><xsl:value-of select="//sf:Phone" /></proposalPreparedByPhone_t>
					<proposalPreparedByCell_t><xsl:value-of select="//sf:MobilePhone" /></proposalPreparedByCell_t>
					<proposalPreparedByEmail_t><xsl:value-of select="//sf:Email" /></proposalPreparedByEmail_t>
				</document>
			</xsl:if>
		</data_xml>
	</xsl:template> 
</xsl:stylesheet> 
