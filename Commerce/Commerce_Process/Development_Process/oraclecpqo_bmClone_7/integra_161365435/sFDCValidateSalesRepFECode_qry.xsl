<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version='1.0'>
	<xsl:output method="xml"/>
	<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
		<!-- Begin SOAP XML -->
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<soap:Header>
				<SessionHeader xmlns="urn:partner.soap.sforce.com">
					<sessionId><xsl:value-of select="/transaction/user_info/session_id"/></sessionId>
				</SessionHeader>
				<CallOptions xmlns="urn:partner.soap.sforce.com">
					<client>BigMachinesLFE/1.0</client>
				</CallOptions>
			</soap:Header>
			<soap:Body>
				<query xmlns="urn:partner.soap.sforce.com">
					<queryString>
						<xsl:choose>
							<xsl:when test="$main_doc/validateSalesRepCode_t = 'Y'">
								Select Name, Region__c,FE__r.Name, FE__c,FE__r.FirstName,FE__r.LastName,FE__r.Phone, FE__r.Email, FE__r.NLS_First_Name__c , FE__r.NLS_Last_Name__c, FE__r.NLS_Language_Code__c, FE__r.Formal_Name__c, FE__r.BigMachines__Login__c, District_Manager__r.BigMachines__Login__c
								from FE_Code__c where Name = '<xsl:value-of select="$main_doc/salesRepCode_quote"/>' 
							</xsl:when>
							<xsl:otherwise>
								Select Name from FE_Code__c where Name = 'XXXXXXXXXXXXXXXXXX'
							</xsl:otherwise>
						</xsl:choose>
					</queryString>
				</query>
			</soap:Body>
		</soap:Envelope>
		<!-- End SOAP XML -->
	</xsl:template>
</xsl:stylesheet>
