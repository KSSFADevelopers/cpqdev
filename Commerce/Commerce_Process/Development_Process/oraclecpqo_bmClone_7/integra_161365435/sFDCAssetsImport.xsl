<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sf2="urn:partner.soap.sforce.com" xmlns:sf="urn:sobject.partner.soap.sforce.com" version="1.0">
<xsl:variable name="main_doc" select="/transaction/data_xml/document[@data_type=0]"/>
<xsl:output method="xml"/>
<xsl:template match="*">
<data_xml>
<document document_var_name="transaction">
<renewalsHolderString_quote>
<xsl:for-each select="//sf2:records">
<xsl:value-of select=".//sf:BigMachines__Part_Number__c"/>
<xsl:text>#</xsl:text>
<xsl:value-of select="./sf:Quantity"/>
<xsl:text>#</xsl:text>
<xsl:value-of select="./sf:InstallDate"/>
<xsl:text>#</xsl:text>
<xsl:value-of select="./sf:UsageEndDate"/>
<xsl:text>#</xsl:text>
<xsl:value-of select="./sf:status"/>
<xsl:text>#</xsl:text>
<xsl:value-of select="./sf:Id"/>
<xsl:text>!!</xsl:text>
</xsl:for-each>
</renewalsHolderString_quote>
</document>
</data_xml>
</xsl:template>
</xsl:stylesheet>